package com.qcmcreator.qcmcreator.payload.request;

import com.qcmcreator.qcmcreator.dao.entity.Result;
import com.qcmcreator.qcmcreator.model.RoleEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SignupRequest {


    private String firstName;

    private String lastName;

    private String email;

    private String password;

    private RoleEnum role;

    private String imgUrl;

    private String address;



}
