package com.qcmcreator.qcmcreator.web.controller;

import com.qcmcreator.qcmcreator.model.QcmDTO;
import com.qcmcreator.qcmcreator.model.QuestionDTO;
import com.qcmcreator.qcmcreator.model.ResultDTO;
import com.qcmcreator.qcmcreator.service.ResultService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("results")
public class ResultRestController {
    private final ResultService resultService;

    public ResultRestController(ResultService resultService) {
        this.resultService = resultService;
    }
    // Method Get to search for all results
    @GetMapping("")
    public List<ResultDTO> findAll(){
        return resultService.findAll();
    }

    // Method Get to search for specific result by id
    @GetMapping("/{resultId}")
    public ResultDTO getResult(@PathVariable Long resultId){
        return resultService.findById(resultId);
    }

    // Method POST to add new result
    @PostMapping("")
    public void addResult(@RequestBody ResultDTO resultDTO){
        resultService.save(resultDTO);
    }

    // Method PUT to update an result by its Id
    @PutMapping("/{resultId}")
    public void updateQcm(@PathVariable Long resultId, @RequestBody ResultDTO resultDTO){
        if( getResult(resultId) != null ){
            resultService.save(resultDTO);
        }
    }

    // Method DELETE to delete a specific result
    @DeleteMapping("/{resultId}")
    public void deleteResult(@PathVariable Long resultId){
        resultService.deleteById(resultId);
    }
}
