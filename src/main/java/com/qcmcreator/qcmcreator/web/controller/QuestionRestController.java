package com.qcmcreator.qcmcreator.web.controller;

import com.qcmcreator.qcmcreator.dao.entity.Question;
import com.qcmcreator.qcmcreator.model.QcmDTO;
import com.qcmcreator.qcmcreator.model.QuestionDTO;
import com.qcmcreator.qcmcreator.service.QuestionService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("questions")
public class QuestionRestController {
    private final QuestionService questionService;

    public QuestionRestController(QuestionService questionService) {
        this.questionService = questionService;
    }

    // Method Get to search for all question
    @PreAuthorize("hasRole('ROLE_STUDENT') or hasRole('ROLE_PROFESSOR')")
    @GetMapping("")
    public List<QuestionDTO> findAll(){
        return questionService.findAll();
    }

    // Method Get to search for specific question by id
    @GetMapping("/{questionId}")
    public QuestionDTO getQuestion(@PathVariable Long questionId){
        return questionService.findById(questionId);
    }

    // Method POST to add new question
    @PostMapping("")
    public void addQuestion(@RequestBody QuestionDTO questionDTO){
        questionService.save(questionDTO);
    }

    // Method PUT to update an question by its Id
    @PutMapping("/{questionId}")
    public void updateQuestion(@PathVariable Long questionId, @RequestBody QuestionDTO questionDTO){
        if( getQuestion(questionId) != null ){
            questionService.save(questionDTO);
        }
    }

    // Method DELETE to delete a specific question
    @DeleteMapping("/{questionId}")
    public void deleteQuestion(@PathVariable Long questionId){
        questionService.deleteById(questionId);
    }
}
