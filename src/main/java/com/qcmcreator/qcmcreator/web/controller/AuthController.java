package com.qcmcreator.qcmcreator.web.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import com.qcmcreator.qcmcreator.dao.entity.User;
import com.qcmcreator.qcmcreator.dao.repository.UserRepository;
import com.qcmcreator.qcmcreator.model.RoleEnum;
import com.qcmcreator.qcmcreator.model.UserDTO;
import com.qcmcreator.qcmcreator.payload.request.LoginRequest;

import com.qcmcreator.qcmcreator.payload.request.SignupRequest;
import com.qcmcreator.qcmcreator.security.jwt.JwtUtils;
import com.qcmcreator.qcmcreator.security.service.UserDetailsImpl;
import com.qcmcreator.qcmcreator.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserService userService;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());



        return ResponseEntity.ok(jwt);
    }


    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) { //sign up

        // Chek if the email is not already signed up
        if (userService.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body("Error: Email is already in use!");
        }

        // Create new user's account
        UserDTO userDTO = UserDTO.builder()
                .firstName(signUpRequest.getFirstName())
                .lastName(signUpRequest.getLastName())
                .email(signUpRequest.getEmail())
                .password(encoder.encode(signUpRequest.getPassword()))
                .imgUrl(signUpRequest.getImgUrl())
                .address(signUpRequest.getAddress())
                .build();

        RoleEnum strRoles = signUpRequest.getRole();

        userDTO.setRole(strRoles);

        userService.save(userDTO);

        return ResponseEntity.ok("User registered successfully!");
    }


}