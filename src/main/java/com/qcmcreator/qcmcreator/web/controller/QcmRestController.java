package com.qcmcreator.qcmcreator.web.controller;

import com.qcmcreator.qcmcreator.model.AnswerDTO;
import com.qcmcreator.qcmcreator.model.QcmDTO;
import com.qcmcreator.qcmcreator.model.UserDTO;
import com.qcmcreator.qcmcreator.service.QcmService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("qcms")
public class QcmRestController {
    private final QcmService qcmService;

    public QcmRestController(QcmService qcmService) {
        this.qcmService = qcmService;
    }


    // Method Get to search for all qcms
    @GetMapping("")
    public List<QcmDTO> findAll(){
        return qcmService.findAll();
    }

    // Method Get to search for specific qcm by id
    @GetMapping("/{qcmId}")
    public QcmDTO getQcm(@PathVariable Long qcmId){
        return qcmService.findById(qcmId);
    }

    // Method POST to add new qcm
    @PostMapping("")
    public void addQcm(@RequestBody QcmDTO qcmDTO){
        qcmService.save(qcmDTO);
    }

    // Method PUT to update an qcm by its Id
    @PutMapping("/{qcmId}")
    public void updateQcm(@PathVariable Long qcmId, @RequestBody QcmDTO qcmDTO){
        if( getQcm(qcmId) != null ){
            qcmService.save(qcmDTO);
        }
    }

    // Method DELETE to delete a specific qcm
    @DeleteMapping("/{qcmId}")
    public void deleteQcm(@PathVariable Long qcmId){
        qcmService.deleteById(qcmId);
    }
}
