package com.qcmcreator.qcmcreator.web.controller;

import com.qcmcreator.qcmcreator.dao.entity.User;
import com.qcmcreator.qcmcreator.model.UserDTO;
import com.qcmcreator.qcmcreator.service.UserService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserRestController {

    private final UserService userService;

    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    // Method Get to search for all users
    @GetMapping("")
    public List<UserDTO> findAll(){
        return userService.findAll();
    }

    // Method Get to search for specific user by id
    @GetMapping("/{userId}")
    public UserDTO getUser(@PathVariable Long userId){
        return userService.findById(userId);
    }

    // Method POST to add new user
    @PostMapping("")
    public void addUser(@RequestBody UserDTO userDTO){
        userService.save(userDTO);
    }

    // Method PUT to update a user by his Id

    @PutMapping("/{userId}")
    public void updateUser(@PathVariable Long userId, @RequestBody UserDTO userDTO){
        if( getUser(userId) != null ){
            userService.save(userDTO);
        }
    }


    // Method DELETE to delete a specific user
    @DeleteMapping("/{userId}")
    public void deleteUser(@PathVariable Long userId){
        userService.deleteById(userId);
    }
}
