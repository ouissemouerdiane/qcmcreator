package com.qcmcreator.qcmcreator.web.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/test")
public class TestController {
    @GetMapping("/all")
    public String allAccess() {
        return "Public Content.";
    }

    @GetMapping("/user")
    @PreAuthorize("hasRole('ROLE_STUDENT')  or hasRole('ROLE_PROFESSOR')")
    public String userAccess() {
        return "All Users are permited to access.";
    }

    @GetMapping("/professor")
    @PreAuthorize("hasRole('ROLE_PROFESSOR')")
    public String moderatorAccess() {
        return "Professor Board.";
    }



    @GetMapping("/student")
    @PreAuthorize("hasRole('ROLE_STUDENT')")
    public String adminAccess() {
        return "Student Board.";
    }
}
