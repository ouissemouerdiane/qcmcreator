package com.qcmcreator.qcmcreator.web.controller;

import com.qcmcreator.qcmcreator.model.AnswerDTO;
import com.qcmcreator.qcmcreator.model.UserDTO;
import com.qcmcreator.qcmcreator.service.AnswerService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/answers")
public class AnswerRestController {

    private final AnswerService answerService;

    public AnswerRestController(AnswerService answerService) {
        this.answerService = answerService;
    }


    // Method Get to search for all answers
    @GetMapping("")
    public List<AnswerDTO> findAll(){
        return answerService.findAll();
    }

    // Method Get to search for specific answer by id
    @GetMapping("/{answerId}")
    public AnswerDTO getAnswer(@PathVariable Long answerId){
        return answerService.findById(answerId);
    }

    // Method POST to add new user
    @PostMapping("")
    public void addAnswer(@RequestBody AnswerDTO answerDTO){
        answerService.save(answerDTO);
    }

    // Method PUT to update an answer by its Id
    @PutMapping("/{answerId}")
    public void updateAnswer(@PathVariable Long answerId, @RequestBody AnswerDTO answerDTO){
        if( getAnswer(answerId) != null ){
            answerService.save(answerDTO);
        }
    }

    // Method DELETE to delete a specific answer
    @DeleteMapping("/{answerId}")
    public void deleteAnswer(@PathVariable Long answerId){
        answerService.deleteById(answerId);
    }
}
