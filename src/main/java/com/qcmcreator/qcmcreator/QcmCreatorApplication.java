package com.qcmcreator.qcmcreator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QcmCreatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(QcmCreatorApplication.class, args);
    }

}
