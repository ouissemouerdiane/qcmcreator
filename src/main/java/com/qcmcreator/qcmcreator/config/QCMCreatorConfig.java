package com.qcmcreator.qcmcreator.config;

import org.modelmapper.ModelMapper;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Component;

@Configuration
public class QCMCreatorConfig {

    @Bean
    public ModelMapper getModelMapper(){
        return new ModelMapper();
    }
}
