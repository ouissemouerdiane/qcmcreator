package com.qcmcreator.qcmcreator.config;


import com.qcmcreator.qcmcreator.dao.entity.User;
import com.qcmcreator.qcmcreator.dao.repository.UserRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

@ConditionalOnProperty(
        value = "configuration.bootstrap.enabled",
        havingValue = "true",
        matchIfMissing = true)
@Component
public class QcmCreatorBootstrap implements ApplicationRunner {

    private final UserRepository userRepository;

    public QcmCreatorBootstrap(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    public void run(ApplicationArguments applicationArguments) {

        User user = User.builder().firstName("test1")
                .email("wouerdiane@gmail.com")
                .lastName("kjbdkfb")
                .password("123456789")
                .address("kjsbkbf").build();
        userRepository.save(user);

    }


}
