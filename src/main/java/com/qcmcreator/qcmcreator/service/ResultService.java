package com.qcmcreator.qcmcreator.service;

import com.qcmcreator.qcmcreator.model.QcmDTO;
import com.qcmcreator.qcmcreator.model.ResultDTO;

import java.util.List;

public interface ResultService {

    public List<ResultDTO> findAll();

    public ResultDTO findById(Long id);

    public void save(ResultDTO newResultDTO);

    public void deleteById(Long id);
}
