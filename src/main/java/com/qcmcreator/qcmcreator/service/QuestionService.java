package com.qcmcreator.qcmcreator.service;


import com.qcmcreator.qcmcreator.model.QuestionDTO;
import com.qcmcreator.qcmcreator.model.UserDTO;

import java.util.List;

public interface QuestionService {
    public List<QuestionDTO> findAll();

    public QuestionDTO findById(Long id);

    public void save(QuestionDTO newQuestionDTO);

    public void deleteById(Long id);
}
