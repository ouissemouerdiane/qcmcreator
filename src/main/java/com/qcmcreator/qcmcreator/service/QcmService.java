package com.qcmcreator.qcmcreator.service;

import com.qcmcreator.qcmcreator.dao.QcmDAO;
import com.qcmcreator.qcmcreator.dao.entity.Qcm;
import com.qcmcreator.qcmcreator.dao.entity.User;
import com.qcmcreator.qcmcreator.model.QcmDTO;

import java.util.List;

public interface QcmService {

    public List<QcmDTO> findAll();

    public QcmDTO findById(Long id);

    public void save(QcmDTO newQcmDTO);

    public void deleteById(Long id);
}
