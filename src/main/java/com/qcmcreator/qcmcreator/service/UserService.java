package com.qcmcreator.qcmcreator.service;

import com.qcmcreator.qcmcreator.model.UserDTO;

import java.util.List;
import java.util.Optional;

public interface UserService {

    public List<UserDTO> findAll();

    public UserDTO findById(Long id);

    public void save(UserDTO newUser);

    public void deleteById(Long id);

    // Authentication methods

    public Optional<UserDTO> findByEmail(String email);

    public Boolean existsByEmail(String email);
}
