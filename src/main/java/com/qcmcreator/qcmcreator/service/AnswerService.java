
package com.qcmcreator.qcmcreator.service;

import com.qcmcreator.qcmcreator.dao.entity.Answer;
import com.qcmcreator.qcmcreator.model.AnswerDTO;

import java.util.List;


public interface AnswerService {
    public List<AnswerDTO> findAll();

    public AnswerDTO findById(Long id);

    public void save(AnswerDTO newAnswerDTO);

    public void deleteById(Long id);
}
