package com.qcmcreator.qcmcreator.service.impl;


import com.qcmcreator.qcmcreator.dao.QuestionDAO;
import com.qcmcreator.qcmcreator.model.QuestionDTO;
import com.qcmcreator.qcmcreator.service.QuestionService;
import com.qcmcreator.qcmcreator.service.mapper.QuestionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {

    private final QuestionDAO questionDAO;
    private final QuestionMapper questionMapper;

    @Autowired
    public QuestionServiceImpl(QuestionDAO questionDAO, QuestionMapper questionMapper) {
        this.questionDAO = questionDAO;
        this.questionMapper = questionMapper;
    }

    @Override
    public List<QuestionDTO> findAll() {
        return questionMapper.mapToListDTO(questionDAO.findAll());
    }

    @Override
    public QuestionDTO findById(Long id) {
        return questionMapper.mapToDTO(questionDAO.findById(id));
    }

    @Override
    public void save(QuestionDTO newQuestionDTO) {
        questionDAO.save(questionMapper.mapToEntity(newQuestionDTO));
    }

    @Override
    public void deleteById(Long id) {
        questionDAO.deleteById(id);

    }
}
