package com.qcmcreator.qcmcreator.service.impl;

import com.qcmcreator.qcmcreator.dao.AnswerDAO;
import com.qcmcreator.qcmcreator.model.AnswerDTO;
import com.qcmcreator.qcmcreator.service.AnswerService;
import com.qcmcreator.qcmcreator.service.mapper.AnswerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AnswerServiceImpl implements AnswerService {

    private final AnswerDAO answerDAO;
    private final AnswerMapper answerMapper;

    @Autowired
    public AnswerServiceImpl(AnswerDAO answerDAO, AnswerMapper answerMapper) {
        this.answerDAO = answerDAO;
        this.answerMapper = answerMapper;
    }

    @Override
    public List<AnswerDTO> findAll() {
        return this.answerMapper.mapToListDTO(answerDAO.findAll());
    }

    @Override
    public AnswerDTO findById(Long id) {
        return this.answerMapper.mapToDTO(answerDAO.findById(id));
    }

    @Override
    public void save(AnswerDTO newAnswerDTO) {
        answerDAO.save(answerMapper.mapToEntity(newAnswerDTO));

    }

    @Override
    public void deleteById(Long id) {
        answerDAO.deleteById(id);
    }
}
