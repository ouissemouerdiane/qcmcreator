package com.qcmcreator.qcmcreator.service.impl;

import com.qcmcreator.qcmcreator.dao.ResultDAO;
import com.qcmcreator.qcmcreator.model.ResultDTO;
import com.qcmcreator.qcmcreator.service.ResultService;
import com.qcmcreator.qcmcreator.service.mapper.ResultMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResultServiceImpl implements ResultService {

    private final ResultDAO resultDAO;
    private final ResultMapper resultMapper;

    public ResultServiceImpl(ResultDAO resultDAO, ResultMapper resultMapper) {
        this.resultDAO = resultDAO;
        this.resultMapper = resultMapper;
    }

    @Override
    public List<ResultDTO> findAll() {
        return resultMapper.mapToListDTO(resultDAO.findAll());
    }

    @Override
    public ResultDTO findById(Long id) {
        return resultMapper.mapToDTO(resultDAO.findById(id));
    }

    @Override
    public void save(ResultDTO newResultDTO) {
        resultDAO.save(resultMapper.mapToEntity(newResultDTO));
    }

    @Override
    public void deleteById(Long id) {
        resultDAO.deleteById(id);
    }
}
