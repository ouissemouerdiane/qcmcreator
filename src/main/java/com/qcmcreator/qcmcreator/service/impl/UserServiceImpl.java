package com.qcmcreator.qcmcreator.service.impl;

import com.qcmcreator.qcmcreator.dao.UserDAO;
import com.qcmcreator.qcmcreator.model.UserDTO;
import com.qcmcreator.qcmcreator.service.UserService;
import com.qcmcreator.qcmcreator.service.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserDAO userDAO;
    private final UserMapper userMapper;

    @Autowired
    public UserServiceImpl(UserDAO userDAO, UserMapper userMapper) {
        this.userDAO = userDAO;
        this.userMapper = userMapper;
    }

    @Override
    public List<UserDTO> findAll() {
        // mapping to dto
        return userMapper.mapToListDTO(this.userDAO.findAll());
    }

    @Override
    public UserDTO findById(Long id) {
        return userMapper.mapToDTO(userDAO.findById(id));
    }

    @Override
    public  void save(UserDTO newUserDTO) {
            userDAO.save(userMapper.mapToEntity(newUserDTO));
    }

    @Override
    public void deleteById(Long id) {
            userDAO.deleteById(id);
    }

    @Override
    public Optional<UserDTO> findByEmail(String email) {
        return Optional.ofNullable(userMapper.mapToDTO(userDAO.findByEmail(email)));
    }

    @Override
    public Boolean existsByEmail(String email) {
        return userDAO.existsByEmail(email);
    }
}
