package com.qcmcreator.qcmcreator.service.impl;

import com.qcmcreator.qcmcreator.dao.QcmDAO;
import com.qcmcreator.qcmcreator.model.QcmDTO;
import com.qcmcreator.qcmcreator.service.QcmService;
import com.qcmcreator.qcmcreator.service.mapper.QcmMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QcmServiceImpl implements QcmService {

    private final QcmDAO qcmDAO;
    private final QcmMapper qcmMapper;

    @Autowired
    public QcmServiceImpl(QcmDAO qcmDAO, QcmMapper qcmMapper) {
        this.qcmDAO = qcmDAO;
        this.qcmMapper = qcmMapper;
    }

    @Override
    public List<QcmDTO> findAll() {
        return qcmMapper.mapToListDTO(qcmDAO.findAll());
    }

    @Override
    public QcmDTO findById(Long id) {
        return qcmMapper.mapToDTO(qcmDAO.findById(id));
    }

    @Override
    public void save(QcmDTO newQcmDTO) {
        qcmDAO.save(qcmMapper.mapToEntity(newQcmDTO));

    }

    @Override
    public void deleteById(Long id) {
        qcmDAO.deleteById(id);

    }
}
