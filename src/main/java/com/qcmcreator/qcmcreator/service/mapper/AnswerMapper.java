package com.qcmcreator.qcmcreator.service.mapper;

import com.qcmcreator.qcmcreator.dao.entity.Answer;
import com.qcmcreator.qcmcreator.dao.entity.User;
import com.qcmcreator.qcmcreator.model.AnswerDTO;
import com.qcmcreator.qcmcreator.model.UserDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class AnswerMapper {
    private final ModelMapper modelMapper;

    @Autowired
    public AnswerMapper(ModelMapper modelMapper){
        this.modelMapper = modelMapper;
    }


    public AnswerDTO mapToDTO(Answer answer){
        AnswerDTO answerDTO = new AnswerDTO();
        this.modelMapper.map(answer, answerDTO); // modelMapper
        return answerDTO;
    }

    public List<AnswerDTO> mapToListDTO(List<Answer> answers){

        return answers.stream().map(answer -> this.mapToDTO(answer)).collect(Collectors.toList());
    }

    public Answer mapToEntity(AnswerDTO answerDTO){
        Answer answer = new Answer();
        this.modelMapper.map(answerDTO, answer);
        return answer;
    }

    public List<Answer> mapToListEntities(List<AnswerDTO> answersDTO){

        return answersDTO.stream().map(answerDTO -> this.mapToEntity(answerDTO)).collect(Collectors.toList());
    }

}
