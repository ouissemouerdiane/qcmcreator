package com.qcmcreator.qcmcreator.service.mapper;

import com.qcmcreator.qcmcreator.dao.entity.User;
import com.qcmcreator.qcmcreator.model.UserDTO;
import org.hibernate.mapping.Collection;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserMapper {


    private final ModelMapper modelMapper;

    @Autowired
    public UserMapper(ModelMapper modelMapper){
        this.modelMapper = modelMapper;
    }

    public UserDTO mapToDTO(User user){
        UserDTO userDto = new UserDTO();
        this.modelMapper.map(user, userDto); // modelMapper
        return userDto;
    }

    public List<UserDTO> mapToListDTO(List<User> users){

        return users.stream().map(user -> this.mapToDTO(user)).collect(Collectors.toList());
    }

    public User mapToEntity(UserDTO userDto){
        User user = new User();
        this.modelMapper.map(userDto, user);
        return user;
    }

    public List<User> mapToListEntities(List<UserDTO> usersDto){

        return usersDto.stream().map(user -> this.mapToEntity(user)).collect(Collectors.toList());
    }



}
