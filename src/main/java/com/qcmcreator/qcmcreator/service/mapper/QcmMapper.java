package com.qcmcreator.qcmcreator.service.mapper;

import com.qcmcreator.qcmcreator.dao.entity.Qcm;
import com.qcmcreator.qcmcreator.dao.entity.User;
import com.qcmcreator.qcmcreator.model.QcmDTO;
import com.qcmcreator.qcmcreator.model.UserDTO;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class QcmMapper {
    private final ModelMapper modelMapper;

    public QcmMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }
    public QcmDTO mapToDTO(Qcm qcm){
        QcmDTO qcmDTO = new QcmDTO();
        this.modelMapper.map(qcm, qcmDTO); // modelMapper
        return qcmDTO;
    }

    public List<QcmDTO> mapToListDTO(List<Qcm> qcms){

        return qcms.stream().map(qcm -> this.mapToDTO(qcm)).collect(Collectors.toList());
    }

    public Qcm mapToEntity(QcmDTO qcmDTO){
        Qcm qcm = new Qcm();
        this.modelMapper.map(qcmDTO, qcm);
        return qcm;
    }

    public List<Qcm> mapToListEntities(List<QcmDTO> qcmsDTO){

        return qcmsDTO.stream().map(qcm -> this.mapToEntity(qcm)).collect(Collectors.toList());
    }

}
