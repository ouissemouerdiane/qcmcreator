package com.qcmcreator.qcmcreator.service.mapper;

import com.qcmcreator.qcmcreator.dao.entity.Result;
import com.qcmcreator.qcmcreator.dao.entity.User;
import com.qcmcreator.qcmcreator.model.ResultDTO;
import com.qcmcreator.qcmcreator.model.UserDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ResultMapper {

    private final ModelMapper modelMapper;

    @Autowired
    public ResultMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }


    public ResultDTO mapToDTO(Result result){
        ResultDTO resultDTO = new ResultDTO();
        this.modelMapper.map(result, resultDTO); // modelMapper
        return resultDTO;
    }

    public List<ResultDTO> mapToListDTO(List<Result> results){

        return results.stream().map(result -> this.mapToDTO(result)).collect(Collectors.toList());
    }

    public Result mapToEntity(ResultDTO resultDTO){
        Result result = new Result();
        this.modelMapper.map(resultDTO, result);
        return result;
    }

    public List<Result> mapToListEntities(List<ResultDTO> resultsDTO){

        return resultsDTO.stream().map(result -> this.mapToEntity(result)).collect(Collectors.toList());
    }

}
