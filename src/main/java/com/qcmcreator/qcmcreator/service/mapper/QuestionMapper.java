package com.qcmcreator.qcmcreator.service.mapper;

import com.qcmcreator.qcmcreator.dao.entity.Question;

import com.qcmcreator.qcmcreator.model.QuestionDTO;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class QuestionMapper {

    private final ModelMapper modelMapper;

    public QuestionMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public QuestionDTO mapToDTO(Question question){
        QuestionDTO questionDTO = new QuestionDTO();
        this.modelMapper.map(question, questionDTO); // modelMapper
        return questionDTO;
    }

    public List<QuestionDTO> mapToListDTO(List<Question> questions){

        return questions.stream().map(question -> this.mapToDTO(question)).collect(Collectors.toList());
    }

    public Question mapToEntity(QuestionDTO questionDTO){
        Question question = new Question();
        this.modelMapper.map(questionDTO, question);
        return question;
    }

    public List<Question> mapToListEntities(List<QuestionDTO> questionsDTO){

        return questionsDTO.stream().map(question -> this.mapToEntity(question)).collect(Collectors.toList());
    }

}
