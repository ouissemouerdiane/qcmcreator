package com.qcmcreator.qcmcreator.model;

import com.qcmcreator.qcmcreator.dao.entity.Question;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class AnswerDTO {

    private Long id;

    private String textAnswer;

    private boolean rightAnswer;

    private Question question;
}
