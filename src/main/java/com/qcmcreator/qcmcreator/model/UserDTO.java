package com.qcmcreator.qcmcreator.model;

import com.qcmcreator.qcmcreator.dao.entity.Result;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



import java.util.Set;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDTO  {

    private Long id;

    private String firstName;

    private String lastName;

    private String email;

    private String password;

    private RoleEnum role;

    private String imgUrl;

    private String address;

    private Set<Result> results;


}
