package com.qcmcreator.qcmcreator.model;

public enum RoleEnum {
    ROLE_STUDENT,
    ROLE_PROFESSOR
}
