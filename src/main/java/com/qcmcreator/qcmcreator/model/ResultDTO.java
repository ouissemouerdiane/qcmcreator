package com.qcmcreator.qcmcreator.model;

import com.qcmcreator.qcmcreator.dao.entity.Qcm;
import com.qcmcreator.qcmcreator.dao.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultDTO {

    private Long id;

    private float mark;

    private User user;

    private Qcm qcm;
}
