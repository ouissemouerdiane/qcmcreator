package com.qcmcreator.qcmcreator.model;

import com.qcmcreator.qcmcreator.dao.entity.Question;
import com.qcmcreator.qcmcreator.dao.entity.Result;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QcmDTO {

    private Long id;

    private String title;

    private String qcmCode;

    private Set<Question> questions;

    private Set<Result> results;
}
