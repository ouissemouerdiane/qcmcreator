package com.qcmcreator.qcmcreator.model;

import com.qcmcreator.qcmcreator.dao.entity.Answer;
import com.qcmcreator.qcmcreator.dao.entity.Qcm;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.util.Set;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionDTO {

    private Long id;

    private String topic;

    private Qcm qcm;

    private Set<Answer> answers;
}
