package com.qcmcreator.qcmcreator.security.service;

import com.qcmcreator.qcmcreator.dao.entity.User;
import com.qcmcreator.qcmcreator.dao.repository.UserRepository;
import com.qcmcreator.qcmcreator.model.UserDTO;
import com.qcmcreator.qcmcreator.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;


import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    UserService userService;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserDTO userDto = userService.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with email: " + email));

        return UserDetailsImpl.build(userDto);
    }

}