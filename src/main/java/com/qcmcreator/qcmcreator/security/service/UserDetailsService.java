package com.qcmcreator.qcmcreator.security.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface UserDetailsService{
       public UserDetails loadUserByEmail(String username) throws UsernameNotFoundException;

}
