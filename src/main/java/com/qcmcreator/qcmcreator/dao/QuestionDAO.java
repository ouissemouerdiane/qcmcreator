package com.qcmcreator.qcmcreator.dao;

import com.qcmcreator.qcmcreator.dao.entity.Question;
import com.qcmcreator.qcmcreator.dao.entity.User;

import java.util.List;

public interface QuestionDAO {

    public List<Question> findAll();

    public Question findById(Long id);

    public void save(Question newQuestion);

    public void deleteById(Long id);
}
