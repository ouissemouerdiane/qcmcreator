package com.qcmcreator.qcmcreator.dao;

import com.qcmcreator.qcmcreator.dao.entity.Answer;
import com.qcmcreator.qcmcreator.dao.entity.User;

import java.util.List;

public interface AnswerDAO {

    public List<Answer> findAll();

    public Answer findById(Long id);

    public void save(Answer newAnswer);

    public void deleteById(Long id);
}
