package com.qcmcreator.qcmcreator.dao.entity;

import com.qcmcreator.qcmcreator.model.RoleEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "users",
        uniqueConstraints = {
        @UniqueConstraint(columnNames = "email")
        })
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column( name = "id")
    private Long id;


    @Size(max = 30)
    @Column(name = "firstname")
    private String firstName;


    @Size(max = 30)
    @Column(name = "lastname")
    private String lastName;

    @Email
    @NotBlank
    @Size(max = 80)
    @Column(name = "email" )
    private String email;

    @NotBlank
    @Column(name = "password")
    private String password;

    @Column(name = "role")
    private RoleEnum role;

    @Column(name = "imgUrl")
    private String imgUrl;

    @Column(name = "address")
    private String address;

    @OneToMany( mappedBy = "user")
    private Set<Result> results;


    @Column(name = "createdAt")
    @CreationTimestamp
    private Date dateCreated;

    @Column(name = "updatedAt")
    @UpdateTimestamp
    private Date lastUpdated;
}
