package com.qcmcreator.qcmcreator.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table ( name = "answers")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Answer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column( name = "id")
    private Long id;

    @Column( name = "answer")
    private String textAnswer;

    @Column( name = "right_answer")
    private boolean rightAnswer;

    @ManyToOne
    @JoinColumn(name = "id_question" , nullable = false)
    private Question question;


    @Column(name = "createdAt")
    @CreationTimestamp
    private Date dateCreated;

    @Column(name = "updatedAt")
    @UpdateTimestamp
    private Date lastUpdated;

}
