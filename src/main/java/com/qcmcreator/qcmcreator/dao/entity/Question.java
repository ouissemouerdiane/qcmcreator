package com.qcmcreator.qcmcreator.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table( name = "questions")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column( name = "id")
    private Long id;

    @Column( name = "topic")
    private String topic;

    @ManyToOne
    @JoinColumn(name = "id_qcm", nullable = false)
    private Qcm qcm;

    @OneToMany(mappedBy = "question")
    private Set<Answer> answers;

    @Column(name = "createdAt")
    @CreationTimestamp
    private Date dateCreated;

    @Column(name = "updatedAt")
    @UpdateTimestamp
    private Date lastUpdated;
}
