package com.qcmcreator.qcmcreator.dao.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = " qcms")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Qcm {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column( name = "id")
    private Long id;

    @Column( name = "title")
    private String title;

    @Column( name = "code")
    private String qcmCode;

    @OneToMany(mappedBy = "qcm")
    private Set<Question> questions;

    @OneToMany(mappedBy = "qcm")
    private Set<Result> results;


    @Column(name = "createdAt")
    @CreationTimestamp
    private Date dateCreated;

    @Column(name = "updatedAt")
    @UpdateTimestamp
    private Date lastUpdated;
}
