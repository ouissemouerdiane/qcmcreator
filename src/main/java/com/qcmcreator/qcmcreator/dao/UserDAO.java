package com.qcmcreator.qcmcreator.dao;

import com.qcmcreator.qcmcreator.dao.entity.User;

import java.util.List;
import java.util.Optional;


public interface UserDAO {

    public List<User> findAll();

    public User findById(Long id);

    public void save(User newUser);

    public void deleteById(Long id);

    // for authentication

    public User findByEmail(String email);

    public Boolean existsByEmail(String email);
}
