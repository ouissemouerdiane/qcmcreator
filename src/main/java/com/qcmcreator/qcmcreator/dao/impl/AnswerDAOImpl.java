package com.qcmcreator.qcmcreator.dao.impl;

import com.qcmcreator.qcmcreator.dao.AnswerDAO;
import com.qcmcreator.qcmcreator.dao.entity.Answer;
import com.qcmcreator.qcmcreator.dao.repository.AnswerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AnswerDAOImpl implements AnswerDAO {

    private final AnswerRepository answerRepository;

    @Autowired
    public AnswerDAOImpl(AnswerRepository answerRepository) {
        this.answerRepository = answerRepository;
    }

    @Override
    public List<Answer> findAll() {
        return answerRepository.findAll();
    }

    @Override
    public Answer findById(Long id) {
        return answerRepository.findById(id).orElse(null);
    }

    @Override
    public void save(Answer newAnswer) {
        answerRepository.save(newAnswer);

    }

    @Override
    public void deleteById(Long id) {
        answerRepository.deleteById(id);
    }
}
