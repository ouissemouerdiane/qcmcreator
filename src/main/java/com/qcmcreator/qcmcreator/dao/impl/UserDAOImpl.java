package com.qcmcreator.qcmcreator.dao.impl;

import com.qcmcreator.qcmcreator.dao.UserDAO;
import com.qcmcreator.qcmcreator.dao.entity.User;
import com.qcmcreator.qcmcreator.dao.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserDAOImpl implements UserDAO {


    private final UserRepository userRepository;

    @Autowired
    public UserDAOImpl(UserRepository userRepository){
        this.userRepository = userRepository;

    }

    @Override
    public List<User> findAll() {

        return userRepository.findAll();
    }

    @Override
    public User findById(Long id) {

        return userRepository.findById(id).orElse(null);
    }

    @Override
    public void save(User newUser) {
        userRepository.save(newUser);

    }

    @Override
    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }

    // Authentication methods

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public Boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }
}
