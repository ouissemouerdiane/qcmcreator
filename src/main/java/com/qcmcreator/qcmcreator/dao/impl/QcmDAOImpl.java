package com.qcmcreator.qcmcreator.dao.impl;

import com.qcmcreator.qcmcreator.dao.QcmDAO;
import com.qcmcreator.qcmcreator.dao.entity.Qcm;
import com.qcmcreator.qcmcreator.dao.repository.QcmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class QcmDAOImpl implements QcmDAO {

    private final QcmRepository qcmRepository;

    @Autowired
    public QcmDAOImpl(QcmRepository qcmRepository) {
        this.qcmRepository = qcmRepository;
    }

    @Override
    public List<Qcm> findAll() {
        return qcmRepository.findAll();
    }

    @Override
    public Qcm findById(Long id) {
        return qcmRepository.findById(id).orElse(null);
    }

    @Override
    public void save(Qcm newQcm) {
        qcmRepository.save(newQcm);

    }

    @Override
    public void deleteById(Long id) {
        qcmRepository.deleteById(id);

    }
}
