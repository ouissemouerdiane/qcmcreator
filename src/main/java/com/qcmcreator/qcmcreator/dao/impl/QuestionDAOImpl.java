package com.qcmcreator.qcmcreator.dao.impl;

import com.qcmcreator.qcmcreator.dao.QuestionDAO;
import com.qcmcreator.qcmcreator.dao.entity.Question;
import com.qcmcreator.qcmcreator.dao.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class QuestionDAOImpl implements QuestionDAO {

    private final QuestionRepository questionRepository;

    @Autowired
    public QuestionDAOImpl(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    @Override
    public List<Question> findAll() {
        return questionRepository.findAll();
    }

    @Override
    public Question findById(Long id) {
        return questionRepository.findById(id).orElse(null);
    }

    @Override
    public void save(Question newQuestion) {
        questionRepository.save(newQuestion);
    }

    @Override
    public void deleteById(Long id) {
        questionRepository.deleteById(id);

    }
}
