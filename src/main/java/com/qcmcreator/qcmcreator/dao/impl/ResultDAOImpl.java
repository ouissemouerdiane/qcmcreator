package com.qcmcreator.qcmcreator.dao.impl;

import com.qcmcreator.qcmcreator.dao.ResultDAO;
import com.qcmcreator.qcmcreator.dao.entity.Result;
import com.qcmcreator.qcmcreator.dao.repository.ResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ResultDAOImpl implements ResultDAO {

    private final ResultRepository resultRepository;
    @Autowired
    public ResultDAOImpl(ResultRepository resultRepository) {
        this.resultRepository = resultRepository;
    }

    @Override
    public List<Result> findAll() {
        return resultRepository.findAll();
    }

    @Override
    public Result findById(Long id) {
        return resultRepository.findById(id).orElse(null);
    }

    @Override
    public void save(Result newResult) {
        resultRepository.save(newResult);
    }

    @Override
    public void deleteById(Long id) {
        resultRepository.deleteById(id);

    }
}
