package com.qcmcreator.qcmcreator.dao.repository;

import com.qcmcreator.qcmcreator.dao.entity.Qcm;
import org.springframework.data.jpa.repository.JpaRepository;


public interface QcmRepository extends JpaRepository<Qcm, Long> {
}


