package com.qcmcreator.qcmcreator.dao.repository;

import com.qcmcreator.qcmcreator.dao.entity.Answer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnswerRepository extends JpaRepository<Answer, Long> {
}


