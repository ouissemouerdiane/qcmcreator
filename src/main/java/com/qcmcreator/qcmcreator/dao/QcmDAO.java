package com.qcmcreator.qcmcreator.dao;

import com.qcmcreator.qcmcreator.dao.entity.Qcm;
import com.qcmcreator.qcmcreator.dao.entity.User;

import java.util.List;

public interface QcmDAO {

    public List<Qcm> findAll();

    public Qcm findById(Long id);

    public void save(Qcm newQcm);

    public void deleteById(Long id);
}
