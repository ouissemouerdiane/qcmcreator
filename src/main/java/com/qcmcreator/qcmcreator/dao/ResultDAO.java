package com.qcmcreator.qcmcreator.dao;

import com.qcmcreator.qcmcreator.dao.entity.Result;
import com.qcmcreator.qcmcreator.dao.entity.User;

import java.util.List;

public interface ResultDAO {

    public List<Result> findAll();

    public Result findById(Long id);

    public void save(Result newResult);

    public void deleteById(Long id);
}
